package com.app.evodream.news

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * @author lukaskris12
 * @since 09/08/18
 */
@GlideModule
class GlideAppModule : AppGlideModule() {
}