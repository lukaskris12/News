package com.app.evodream.news

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import com.app.evodream.news.ui.source.SourceFragment
import com.app.evodream.news.util.AndroidUtil
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AndroidUtil.switchToInanimate(R.id.fragment_container, SourceFragment.newInstance(), supportFragmentManager)
    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector
}
