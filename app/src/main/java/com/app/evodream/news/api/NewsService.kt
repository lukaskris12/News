package com.app.evodream.news.api

import com.app.evodream.news.model.HeadlineResponse
import com.app.evodream.news.model.SourceResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author lukaskris12
 * @since 09/08/18
 */
interface NewsService{
    @GET("sources")
    fun readNews(
            @Query("language") language: String,
            @Query("apiKey") apiKey: String
    ): Call<SourceResponse>

    @GET("everything")
    fun readTopHeadlines(
            @Query("sources") sources: String,
            @Query("q") search: String,
            @Query("apiKey") apiKey: String,
            @Query("page") page: Int,
            @Query("pageSize") pageSize: Int
    ): Call<HeadlineResponse>
}