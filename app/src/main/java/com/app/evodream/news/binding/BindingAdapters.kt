package com.app.evodream.news.binding

import android.content.Context
import android.databinding.BindingAdapter
import android.view.View
import android.widget.ImageView
import com.app.evodream.news.GlideApp
import com.app.evodream.news.R

/**
 * Data Binding adapters specific to the app.
 */
object BindingAdapters {
    @JvmStatic
    @BindingAdapter("visibleGone")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }
    @JvmStatic
    @BindingAdapter("imageUrl")
    fun bindImage(imageView: ImageView, url: String?) {
        GlideApp.with(imageView.context).load(url).centerCrop().thumbnail(getPlaceholder(imageView.context)).into(imageView)
    }

    private fun getPlaceholder(context: Context) = GlideApp.with(context).load(R.drawable.placeholder_double_ring)
}
