package com.app.evodream.news.datasource

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import com.app.evodream.news.BuildConfig
import com.app.evodream.news.api.NewsService
import com.app.evodream.news.model.Headline
import com.app.evodream.news.model.HeadlineResponse
import com.app.evodream.news.model.NetworkState
import com.app.evodream.news.model.Source
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber


class NewsDataSource (private val newsService: NewsService, private val compositeDisposable: CompositeDisposable) : PageKeyedDataSource<Long,Headline>() {
    private val networkState = MutableLiveData<NetworkState>()
    private val initialLoading = MutableLiveData<NetworkState>()
    private lateinit var source: Source
    private lateinit var queryString: String

    fun getNetworkState() = networkState

    fun getInitialLoading() = initialLoading

    /**
     * Keep Completable reference for the try event
     */
    private var retryCompletable: Completable? = null

    fun setFilter(source: Source, queryString: String){
        this.source = source
        this.queryString = queryString
    }

    fun retry(){
        retryCompletable?.let {
            compositeDisposable.add(
                    it.subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {},
                                    {
                                        throwable->
                                        Timber.e(throwable.message)
                                    }
                            )
            )
        }
    }

    override fun loadInitial(params: LoadInitialParams<Long>, callback: LoadInitialCallback<Long, Headline>) {
        initialLoading.postValue(NetworkState.LOADING)
        networkState.postValue(NetworkState.LOADING)

        newsService.readTopHeadlines(source.id, queryString, BuildConfig.ApiKey, 1, params.requestedLoadSize).enqueue(object : Callback<HeadlineResponse> {
            override fun onFailure(call: Call<HeadlineResponse>?, t: Throwable?) {
                setRetry { loadInitial(params, callback) }
                val errorMessage = t?.message ?: "unknown error"
                networkState.postValue(NetworkState(NetworkState.Status.FAILED, errorMessage))
                initialLoading.postValue(NetworkState(NetworkState.Status.FAILED, errorMessage))
            }

            override fun onResponse(call: Call<HeadlineResponse>, response: Response<HeadlineResponse>) {
                // Clear retry since last request succeeded
                setRetry(null)
                if (response.isSuccessful) {
                    response.body()?.articles?.let {
                        if(it.isEmpty()){
                            val error = "Pencarian terhadap ${queryString} tidak ditemukan."
                            initialLoading.postValue(NetworkState(NetworkState.Status.FAILED, error))
                            networkState.postValue(NetworkState(NetworkState.Status.FAILED, error))
                        }else{
                            callback.onResult(it, null, 21)
                            initialLoading.postValue(NetworkState.LOADED)
                            networkState.postValue(NetworkState.LOADED)
                        }
                    }
                } else {
                    initialLoading.postValue(NetworkState(NetworkState.Status.FAILED, response.message()))
                    networkState.postValue(NetworkState(NetworkState.Status.FAILED, response.message()))
                }
            }
        })

    }
  /*
   * This method it is responsible for the subsequent call to load the data page wise.
   * This method is executed in the background thread
   * We are fetching the next page data from the api
   * and passing it via the callback method to the UI.
   * The "params.key" variable will have the updated value.
   */
    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, Headline>) {
        Timber.i("Loading Rang " + params.key + " count " + params.requestedLoadSize)
        networkState.postValue(NetworkState.LOADING)
        newsService.readTopHeadlines(source.id, queryString, BuildConfig.ApiKey, params.key.toInt(), params.requestedLoadSize).enqueue(object : Callback<HeadlineResponse> {
              override fun onResponse(call: Call<HeadlineResponse>, response: Response<HeadlineResponse>) {
                  /*
                 * If the request is successful, then we will update the callback
                 * with the latest feed items and
                 * "params.key+1" -> set the next key for the next iteration.
                 */
                  if (response.isSuccessful) {
                      setRetry(null)
                      response.body()?.let {
                          val nextKey = if(params.key.toInt() == it.totalResults) null else params.key+1
                          callback.onResult(it.articles, nextKey)
                          networkState.postValue(NetworkState.LOADED)
                      }
                  } else {
                      setRetry { loadAfter(params, callback) }
                      networkState.postValue(NetworkState(NetworkState.Status.FAILED, response.message()))
                  }
              }

              override fun onFailure(call: Call<HeadlineResponse>?, t: Throwable?) {
                  setRetry { loadAfter(params, callback) }
                  val errorMessage = t?.message ?: "unknown error"
                  networkState.postValue(NetworkState(NetworkState.Status.FAILED, errorMessage))
              }
        })
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, Headline>) {}

    private fun setRetry(action: (() -> Unit)?){
        retryCompletable = if(action == null){
            null
        }else{
            Completable.fromAction(action)
        }
    }
}