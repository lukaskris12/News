package com.app.evodream.news.datasource

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.app.evodream.news.api.NewsService
import com.app.evodream.news.model.Headline
import com.app.evodream.news.model.Source
import io.reactivex.disposables.CompositeDisposable

class NewsDataSourceFactory(
        private val service: NewsService,
        private val compositeDisposable: CompositeDisposable,
        private val source: Source,
        private val queryString: String
) : DataSource.Factory<Long, Headline>(){

    private val dataSourceLiveData = MutableLiveData<NewsDataSource>()
    override fun create(): DataSource<Long, Headline> {
        val newsDataSource = NewsDataSource(service, compositeDisposable)
        newsDataSource.setFilter(source, queryString)
        dataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }

    fun getNewsDataSourceLiveData() = dataSourceLiveData
}