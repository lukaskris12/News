package com.app.evodream.news.di

import com.app.evodream.news.ui.news.HeadlineFragment
import com.app.evodream.news.ui.source.SourceFragment
import com.app.evodream.news.ui.webview.WebviewFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author lukaskris12
 * @since 09/08/18
 */

@Suppress("unused")
@Module
abstract class FragmentBuildersModule{
    @ContributesAndroidInjector
    abstract fun contributeSourceFragment(): SourceFragment

    @ContributesAndroidInjector
    abstract fun contributeHeadlineFragment(): HeadlineFragment

    @ContributesAndroidInjector
    abstract fun contributeWebViewFragment(): WebviewFragment
}