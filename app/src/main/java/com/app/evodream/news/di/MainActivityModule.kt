package com.app.evodream.news.di

import com.app.evodream.news.MainActivity
import com.app.evodream.news.ui.news.HeadlineActivity
import com.app.evodream.news.ui.webview.WebviewActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author lukaskris12
 * @since 09/08/18
 */

@Suppress("unused")
@Module
abstract class MainActivityModule{

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeHeadlineActivity(): HeadlineActivity

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeWebViewActivity(): WebviewActivity

}