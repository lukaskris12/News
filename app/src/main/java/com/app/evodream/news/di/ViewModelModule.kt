package com.app.evodream.news.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.app.evodream.news.ui.news.HeadlineViewModel
import com.app.evodream.news.ui.source.SourceViewModel
import com.app.evodream.news.viewmodel.AppViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * @author lukaskris12
 * @since 09/08/18
 */

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SourceViewModel::class)
    abstract fun bindSourceViewModel(sourceViewModel: SourceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HeadlineViewModel::class)
    abstract fun bindHeadlineViewModel(headlineViewModel: HeadlineViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModel): ViewModelProvider.Factory
}