package com.app.evodream.news.ext

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Transformations
import com.app.evodream.news.util.AbsentLiveData


fun <X, Y> LiveData<X>?.map(transform: (data: X) -> Y): LiveData<Y> {
    return if (this == null) AbsentLiveData.create()
    else Transformations.map(this) { data: X? ->
        if (data == null) null else transform.invoke(data)
    }
}

fun <X, Y> LiveData<X>?.switch(transform: (data: X) -> LiveData<Y>): LiveData<Y> {
    return if (this == null) AbsentLiveData.create()
    else Transformations.switchMap(this) { data: X? ->
        if (data == null) {
            AbsentLiveData.create()
        } else {
            transform.invoke(data)
        }
    }
}

fun <X> MediatorLiveData<X>.source(liveData: LiveData<X>) {
    addSource(liveData) { value = it }
}