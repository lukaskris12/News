package com.app.evodream.news.ext

import com.app.evodream.news.model.Resource
import com.app.evodream.news.model.Status

fun <T> Resource<T>?.successAvailable(callback: (T) -> Unit) {
    if (this != null && status == Status.SUCCESS && data != null) {
        callback.invoke(this.data)
    }
}

fun <T> Resource<T>?.available(callback: (T) -> Unit) {
    if (this != null && data != null) {
        callback.invoke(this.data)
    }
}

fun <T> Resource<T>?.error(callback: (String) -> Unit) {
    if (this != null && status == Status.ERROR && message != null) {
        callback.invoke(this.message)
    }
}

fun <T> Resource<T>?.loading(callback: () -> Unit) {
    if (this != null && status == Status.LOADING) {
        callback.invoke()
    }
}