package com.app.evodream.news.ext

import android.content.Context
import android.os.SystemClock
import android.view.View
import android.widget.Toast

fun String.toast(context: Context?, length: Int = Toast.LENGTH_LONG) {
    Toast.makeText(context, this, length).show()
}

fun View.setOnSafeClickListener(handler: (View) -> Unit) {
    setOnClickListener {
        if (SystemClock.elapsedRealtime() - (it.tag as Long? ?: 0) >= 100){
            handler.invoke(this)
            it.tag = SystemClock.elapsedRealtime()
        }
    }
}