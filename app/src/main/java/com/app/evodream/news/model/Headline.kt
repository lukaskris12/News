package com.app.evodream.news.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*


class Headline (
        var source: Source = Source(),
        var author: String = "",
        var title: String = "",
        var description: String = "",
        var url: String = "",
        @SerializedName("urlToImage")
        var urlToImage: String = "",
        var publishedAt: Date = Date()
): Serializable