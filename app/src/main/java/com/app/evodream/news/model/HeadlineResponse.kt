package com.app.evodream.news.model

import java.io.Serializable

class HeadlineResponse (
        var status: String = "",
        var totalResults: Int = 0,
        var articles: List<Headline> = ArrayList()
): Serializable