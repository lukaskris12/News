package com.app.evodream.news.model

data class NetworkState (private val status: Status,
                    private val message: String) {
    enum class Status{
        RUNNING,
        SUCCESS,
        FAILED
    }

    companion object {
        @JvmStatic val LOADED: NetworkState = NetworkState(Status.SUCCESS, "Success")
        @JvmStatic val LOADING: NetworkState = NetworkState(Status.RUNNING, "Running")
    }

    fun getStatus()= status
    fun getMessage() = message



}

fun NetworkState?.successAvailable(callback: () -> Unit) {
    if (this != null && this.getStatus() == NetworkState.Status.SUCCESS) {
        this.let { callback.invoke() }
    }
}

fun NetworkState?.error(callback: (String) -> Unit) {
    if (this != null && this.getStatus() == NetworkState.Status.FAILED && this.getMessage() != null) {
        this.getMessage().let { callback.invoke(it) }
    }
}

fun NetworkState?.loading(callback: () -> Unit) {
    if (this != null && this.getStatus() == NetworkState.Status.RUNNING) {
        callback.invoke()
    }
}