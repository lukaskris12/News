package com.app.evodream.news.model

import java.io.Serializable

/**
 * @author lukaskris12
 * @since 09/08/18
 */
class Source(var id: String = "",
             var name: String = "",
             var description: String = "",
             var url: String = "",
             var category: String = "",
             var language: String = "",
             var country: String = "") : Serializable