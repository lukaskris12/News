package com.app.evodream.news.model

class SourceResponse(
        var status: String = "",
        var totalResults: Int = 0,
        var sources: List<Source> = ArrayList()
) {
}