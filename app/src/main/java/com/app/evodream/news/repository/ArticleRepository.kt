package com.app.evodream.news.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.app.evodream.news.api.NewsService
import com.app.evodream.news.datasource.NewsDataSource
import com.app.evodream.news.datasource.NewsDataSourceFactory
import com.app.evodream.news.model.Headline
import com.app.evodream.news.model.HeadlineResponse
import com.app.evodream.news.model.Resource
import com.app.evodream.news.model.Source
import com.app.evodream.news.util.Listing
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class ArticleRepository @Inject constructor(
        private val newsService: NewsService
){

    companion object {
        private const val pageSize = 20
    }

    private val compositeDisposable = CompositeDisposable()
    private lateinit var dataSourceFactory: NewsDataSourceFactory


    fun get(source: Source, search: String, apiKey: String, offset: Int=1, limit: Int=20): LiveData<Resource<List<Headline>>> {
        return MutableLiveData<Resource<List<Headline>>>().apply {
            value = Resource.loading(null)
            newsService.readTopHeadlines(source.id, search, apiKey, offset / limit + 1, limit).enqueue(object : Callback<HeadlineResponse> {
                override fun onFailure(call: Call<HeadlineResponse>?, t: Throwable?) {
                    value = Resource.error(t.toString(), null)
                }

                override fun onResponse(call: Call<HeadlineResponse>, response: Response<HeadlineResponse>) {
                    value = if (response.isSuccessful) {
                        Resource.success(response.body()?.articles)
                    } else {
                        Resource.error("Error Load Data", null)
                    }
                }
            })
        }
    }

    fun getList(source: Source, search: String): Listing<Headline>{
        dataSourceFactory = NewsDataSourceFactory(newsService, compositeDisposable, source, search)
        val config = PagedList.Config.Builder()
                .setPageSize(pageSize)
                .setInitialLoadSizeHint(pageSize * 2)
                .setEnablePlaceholders(true)
                .build()
        val pagedList: LiveData<PagedList<Headline>> = LivePagedListBuilder(dataSourceFactory, config).build()
        return Listing(
                pagedList,
                Transformations.switchMap(dataSourceFactory.getNewsDataSourceLiveData(), NewsDataSource::getInitialLoading),
                Transformations.switchMap(dataSourceFactory.getNewsDataSourceLiveData(), NewsDataSource::getNetworkState),
                Transformations.switchMap(dataSourceFactory.getNewsDataSourceLiveData(), NewsDataSource::getInitialLoading)
        )
    }

    fun retry() = dataSourceFactory.getNewsDataSourceLiveData().value?.retry()

    fun refresh() = dataSourceFactory.getNewsDataSourceLiveData().value?.invalidate()

    fun clear() = compositeDisposable.dispose()
}