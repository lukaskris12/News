package com.app.evodream.news.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.app.evodream.news.api.NewsService
import com.app.evodream.news.model.Resource
import com.app.evodream.news.model.Source
import com.app.evodream.news.model.SourceResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author lukaskris12
 * @since 09/08/18
 */
@Singleton
class SourceRepository @Inject constructor(
        private val newsService: NewsService
){
    fun get(apiKey: String): LiveData<Resource<List<Source>>>{
        return MutableLiveData<Resource<List<Source>>>().apply {
            value = Resource.loading(null)
            newsService.readNews("en", apiKey).enqueue(object : Callback<SourceResponse>{
                override fun onFailure(call: Call<SourceResponse>?, t: Throwable?) {
                    value = Resource.error(t.toString(), null)
                }

                override fun onResponse(call: Call<SourceResponse>, response: Response<SourceResponse>) {
                    value = if(response.isSuccessful){
                        Resource.success(response.body()?.sources)
                    }else{
                        Resource.error("Error Load Data", null)
                    }
                }
            })
        }
    }
}