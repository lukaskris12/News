package com.app.evodream.news.ui.common

interface ClickCallback<T> {
    fun click(obj: T)
}
