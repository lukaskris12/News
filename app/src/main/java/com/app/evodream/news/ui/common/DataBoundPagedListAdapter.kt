package com.app.evodream.news.ui.common

import android.arch.paging.PagedListAdapter
import android.databinding.ViewDataBinding
import android.support.v7.recyclerview.extensions.AsyncDifferConfig
import android.support.v7.util.DiffUtil
import android.view.ViewGroup
import com.app.evodream.news.AppExecutors
import com.app.evodream.news.model.NetworkState

abstract class DataBoundPagedListAdapter <T, V : ViewDataBinding>(
        appExecutors: AppExecutors,
        diffCallback: DiffUtil.ItemCallback<T>
) : PagedListAdapter<T, DataBoundViewHolder<V>>(
        AsyncDifferConfig.Builder<T>(diffCallback)
                .setBackgroundThreadExecutor(appExecutors.diskIO())
                .build()
) {
    private var networkState: NetworkState? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBoundViewHolder<V> {
        val binding = createBinding(parent)
        return DataBoundViewHolder(binding)
    }

    protected abstract fun createBinding(parent: ViewGroup): V

    override fun onBindViewHolder(holder: DataBoundViewHolder<V>, position: Int) {
        getItem(position)?.let {
            bind(holder.binding, it)
            holder.binding.executePendingBindings()
        }
    }

    protected abstract fun bind(binding: V, item: T)

//    override fun getItemCount(): Int = super.getItemCount() + if(hasExtraRow()) 1 else 0

    fun setNetworkState(networkState: NetworkState){
        currentList?.let {
            if(it.size != 0){
                val previousState = networkState
                val hadExtraRow = hasExtraRow()
                this.networkState = networkState
                val hasExtraRow = hasExtraRow()
                if(hadExtraRow != hasExtraRow){
                    if(hadExtraRow) notifyItemRemoved(super.getItemCount())
                    else notifyItemInserted(super.getItemCount())
                }else if(hasExtraRow && previousState != networkState){
                    notifyItemChanged(itemCount - 1)
                }
            }

        }
    }
    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED
}