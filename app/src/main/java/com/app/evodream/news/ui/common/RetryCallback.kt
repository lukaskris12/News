package com.app.evodream.news.ui.common

interface RetryCallback {
    fun retry()
}