package com.app.evodream.news.ui.news

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.app.evodream.news.R
import com.app.evodream.news.model.Source
import com.app.evodream.news.util.AndroidUtil
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class HeadlineActivity : AppCompatActivity(), HasSupportFragmentInjector{
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    companion object {
        private const val EXTRA_SOURCE = "EXTRA_SOURCE"
        fun getStartIntent(context: Context, source: Source) = Intent(context, HeadlineActivity::class.java).apply {
            putExtra(EXTRA_SOURCE, source)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.container)
        val source = intent.getSerializableExtra(EXTRA_SOURCE) as Source
        supportActionBar?.title = source.name
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        AndroidUtil.switchToInanimate(R.id.fragment_container, HeadlineFragment.newInstance(source), supportFragmentManager)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId){
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
}