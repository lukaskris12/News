package com.app.evodream.news.ui.news

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.evodream.news.AppExecutors
import com.app.evodream.news.R
import com.app.evodream.news.databinding.HeadlineFragmentBinding
import com.app.evodream.news.di.Injectable
import com.app.evodream.news.ext.error
import com.app.evodream.news.ext.successAvailable
import com.app.evodream.news.ext.toast
import com.app.evodream.news.model.*
import com.app.evodream.news.ui.common.RetryCallback
import com.app.evodream.news.ui.webview.WebviewActivity
import com.app.evodream.news.util.ExtendedFragment
import com.app.evodream.news.util.autoCleared
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class HeadlineFragment : ExtendedFragment(), Injectable{
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    var binding by autoCleared<HeadlineFragmentBinding>()
    var adapter by autoCleared<HeadlinePagedListAdapter>()

    lateinit var viewModel: HeadlineViewModel

    private lateinit var disposable: Disposable

    companion object {
        private const val EXTRA_SOURCE = "EXTRA_SOURCE"
        fun newInstance(source: Source) = HeadlineFragment().apply { arguments = Bundle().apply { putSerializable(EXTRA_SOURCE, source) } }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.headline_fragment, container ,false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HeadlineViewModel::class.java)
        viewModel.setSource(arguments?.getSerializable(EXTRA_SOURCE) as Source)
        initRecyclerView()

        adapter = HeadlinePagedListAdapter(appExecutors){headline->
            context?.let {
                startActivity(WebviewActivity.getStartIntent(it, headline))
            }
        }
        binding.headlines.adapter = adapter

        binding.callback = object : RetryCallback {
            override fun retry() {
                viewModel.reload()
            }
        }

        disposable = setTextWatcher().subscribeOn(Schedulers.single()).observeOn(AndroidSchedulers.mainThread()).subscribe{
            viewModel.search(it)
        }


    }

    private fun setTextWatcher()=
        Observable.create <String>{ emitter->
            val textWatcher = object: TextWatcher{
                override fun afterTextChanged(s: Editable?) {}

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    s?.toString()?.let { emitter.onNext(it) }
                }
            }
            binding.search.addTextChangedListener(textWatcher)
            emitter.setCancellable { binding.search.removeTextChangedListener(textWatcher) }
        }.filter{ it.isNotEmpty() }.debounce(1000, TimeUnit.MILLISECONDS)


    private fun initRecyclerView(){
        viewModel.newsList.observe(this, Observer {listing ->
            listing?.initialState?.observe(this, Observer {
                it?.loading { binding.resource = Resource.loading(null) }
                it?.successAvailable { binding.resource = Resource.success(adapter.currentList) }
                it?.error { error -> binding.resource = Resource.error(error, null) }
            })
            listing?.networkState?.observe(this, Observer {

            })
            listing?.pagedList?.observe(this, Observer {
                adapter.submitList(it)
            })
        })
    }
}