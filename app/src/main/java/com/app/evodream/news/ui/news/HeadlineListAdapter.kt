package com.app.evodream.news.ui.news

import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.evodream.news.AppExecutors
import com.app.evodream.news.R
import com.app.evodream.news.databinding.HeadlineItemBinding
import com.app.evodream.news.model.Headline
import com.app.evodream.news.ui.common.DataBoundListAdapter

class HeadlineListAdapter (
        appExecutors: AppExecutors,
        private val clickCallback: ((Headline) -> Unit)?
) : DataBoundListAdapter<Headline, HeadlineItemBinding>(
        appExecutors = appExecutors,
        diffCallback = object : DiffUtil.ItemCallback<Headline>() {
            override fun areItemsTheSame(oldItem: Headline, newItem: Headline): Boolean {
                return oldItem.url == newItem.url
            }

            override fun areContentsTheSame(oldItem: Headline, newItem: Headline): Boolean {
                return oldItem.title == newItem.title
                        && oldItem.description == newItem.description
            }
        }
){
    override fun createBinding(parent: ViewGroup): HeadlineItemBinding {
        val binding = DataBindingUtil.inflate<HeadlineItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.headline_item,
                parent,
                false
        )
        binding.root.setOnClickListener {
            binding.article?.let {
                clickCallback?.invoke(it)
            }
        }
        return binding
    }

    override fun bind(binding: HeadlineItemBinding, item: Headline) {
        binding.article = item
    }
}