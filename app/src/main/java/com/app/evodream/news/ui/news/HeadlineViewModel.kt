package com.app.evodream.news.ui.news

import android.app.Application
import android.arch.lifecycle.*
import android.arch.paging.PagedList
import com.app.evodream.news.R
import com.app.evodream.news.ext.*
import com.app.evodream.news.model.Headline
import com.app.evodream.news.model.NetworkState
import com.app.evodream.news.model.Resource
import com.app.evodream.news.model.Source
import com.app.evodream.news.repository.ArticleRepository
import com.app.evodream.news.util.Listing
import javax.inject.Inject

class HeadlineViewModel @Inject constructor(app: Application, private val articleRepository: ArticleRepository) : ViewModel(){

    private lateinit var source: Source
    private val query =  MutableLiveData<String>()

    val newsList = query.switch {
        newsListing = articleRepository.getList(source, it)
        MutableLiveData<Listing<Headline>>().apply {
            value = newsListing
        }
    }

    private lateinit var newsListing: Listing<Headline>

    fun setSource(source: Source){
        this.source = source
        search("")
    }

    fun search(text: String){
        query.value = text
    }

    fun reload(){
        search("")
    }

}