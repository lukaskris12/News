package com.app.evodream.news.ui.source

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.evodream.news.AppExecutors
import com.app.evodream.news.R
import com.app.evodream.news.databinding.SourceFragmentBinding
import com.app.evodream.news.di.Injectable
import com.app.evodream.news.ext.error
import com.app.evodream.news.ext.toast
import com.app.evodream.news.ui.common.RetryCallback
import com.app.evodream.news.ui.news.HeadlineActivity
import com.app.evodream.news.util.autoCleared
import com.app.evodream.news.util.ExtendedFragment
import javax.inject.Inject

class SourceFragment : ExtendedFragment(), Injectable{
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    var binding by autoCleared<SourceFragmentBinding>()
    var adapter by autoCleared<SourceListAdapter>()

    lateinit var viewModel: SourceViewModel

    companion object {
        fun newInstance() = SourceFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.source_fragment, container ,false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SourceViewModel::class.java)
        initRecyclerView()

        adapter = SourceListAdapter(appExecutors){
            context?.apply{ startActivity(HeadlineActivity.getStartIntent(this, it)) }
        }
        binding.sources.adapter = adapter

        binding.callback = object : RetryCallback{
            override fun retry() {
                viewModel.reload()
            }
        }
    }

    private fun initRecyclerView(){
        viewModel.results.observe(this, Observer { result->
            binding.resource = result
            adapter.submitList(result?.data)
            result.error {
                it.toast(context)
            }
        })
    }
}