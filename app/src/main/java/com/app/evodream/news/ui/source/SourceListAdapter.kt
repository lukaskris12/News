package com.app.evodream.news.ui.source

import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.app.evodream.news.AppExecutors
import com.app.evodream.news.R
import com.app.evodream.news.databinding.SourceItemBinding
import com.app.evodream.news.model.Source
import com.app.evodream.news.ui.common.DataBoundListAdapter

class SourceListAdapter(
        appExecutors: AppExecutors,
        private val clickCallback: ((Source) -> Unit)?
) : DataBoundListAdapter<Source, SourceItemBinding>(
        appExecutors = appExecutors,
        diffCallback = object : DiffUtil.ItemCallback<Source>() {
            override fun areItemsTheSame(oldItem: Source, newItem: Source): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Source, newItem: Source): Boolean {
                return oldItem.name == newItem.name
                        && oldItem.id == newItem.id
            }
        }
){
    override fun createBinding(parent: ViewGroup): SourceItemBinding {
        val binding = DataBindingUtil.inflate<SourceItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.source_item,
                parent,
                false
        )
        binding.root.setOnClickListener {
            binding.source?.let {
                clickCallback?.invoke(it)
            }
        }
        return binding
    }

    override fun bind(binding: SourceItemBinding, item: Source) {
        binding.source = item
    }
}