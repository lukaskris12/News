package com.app.evodream.news.ui.source

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.app.evodream.news.R
import com.app.evodream.news.repository.SourceRepository
import javax.inject.Inject

class SourceViewModel @Inject constructor(private val app: Application, private val sourceRepository: SourceRepository) : ViewModel(){
    val queryData = MutableLiveData<String>()
    val results = Transformations.switchMap(queryData){
        sourceRepository.get(app.getString(R.string.api_key))
    }

    init {
        reload()
    }

    fun reload(){
        queryData.value = ""
    }
}