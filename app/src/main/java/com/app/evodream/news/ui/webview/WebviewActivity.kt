package com.app.evodream.news.ui.webview

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.app.evodream.news.R
import com.app.evodream.news.model.Headline
import com.app.evodream.news.util.AndroidUtil
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


class WebviewActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>


    companion object {
        private const val EXTRA_URL = "EXTRA_URL"
        fun getStartIntent(context: Context, article: Headline) = Intent(context, WebviewActivity::class.java).apply {
            putExtra(EXTRA_URL, article)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.container)
        val headline = intent.getSerializableExtra(EXTRA_URL) as Headline
        supportActionBar?.title = headline.title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        AndroidUtil.switchToInanimate(R.id.fragment_container, WebviewFragment.newInstance(headline.url), supportFragmentManager)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId){
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector
}