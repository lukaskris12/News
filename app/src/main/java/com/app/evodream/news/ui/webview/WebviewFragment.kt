package com.app.evodream.news.ui.webview

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import com.app.evodream.news.R
import com.app.evodream.news.databinding.WebViewBinding
import com.app.evodream.news.di.Injectable
import com.app.evodream.news.util.ExtendedFragment
import com.app.evodream.news.util.autoCleared

/**
 * @author giorgio
 * @since 11/08/18
 */
class WebviewFragment : ExtendedFragment(), Injectable {

    var binding by autoCleared<WebViewBinding>()

    companion object {
        private const val EXTRA_URL = "EXTRA_URL"
        fun newInstance(url: String) = WebviewFragment().apply { arguments = Bundle().apply { putSerializable(EXTRA_URL, url) } }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.web_view, container ,false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.webview.webViewClient = WebViewClient()
        binding.webview.loadUrl(arguments?.getSerializable(EXTRA_URL) as String)
    }
}