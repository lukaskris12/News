package com.app.evodream.news.util

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

class AndroidUtil {
    companion object {

        @JvmStatic
        fun switchToInanimate(containerResId: Int, fragment: Fragment, fragmentManager: FragmentManager) {
            fragmentManager
                    .beginTransaction()
                    .replace(containerResId, fragment)
                    .commit()
        }

        @JvmStatic
        fun switchTo(containerResId: Int, fragment: Fragment, enterAnimResId: Int, exitAnimResId: Int, fragmentManager: FragmentManager) {
            fragmentManager
                    .beginTransaction()
                    .setCustomAnimations(enterAnimResId, exitAnimResId)
                    .replace(containerResId, fragment)
                    .commit()
        }
    }
}