package com.app.evodream.news.util

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import com.app.evodream.news.model.NetworkState

class Listing<T>(
        val pagedList: LiveData<PagedList<T>>,
        val initialState: LiveData<NetworkState>,
        val networkState: LiveData<NetworkState>,
        val refreshState: LiveData<NetworkState>
)